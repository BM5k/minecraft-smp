

advancement grant @s[score_hc_witch_min=25] only hermitcraft:impulse/witch_farm
advancement grant @s[score_hc_guardian_min=50] only hermitcraft:impulse/guardian_slayer
advancement grant @s[lm=100] only hermitcraft:impulse/experienced_farmer

advancement revoke @s[score_hc_witch=24] only hermitcraft:impulse/impulse_all_trigger
advancement revoke @s[score_hc_guardian=49] only hermitcraft:impulse/impulse_all_trigger
advancement revoke @s[l=99] only hermitcraft:impulse/impulse_all_trigger
