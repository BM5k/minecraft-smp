
summon area_effect_cloud ~-5 ~-5 ~-5 {Tags:["hc_test_beacon"],Particle:"take"}

scoreboard players set @e[type=area_effect_cloud,c=1,tag=hc_test_beacon] hc_beacon_pos_x -5
scoreboard players set @e[type=area_effect_cloud,c=1,tag=hc_test_beacon] hc_beacon_pos_y -5
scoreboard players set @e[type=area_effect_cloud,c=1,tag=hc_test_beacon] hc_beacon_pos_z -5

execute @e[type=area_effect_cloud,c=1,tag=hc_test_beacon] ~ ~ ~ function hermitcraft:python/emerald_beacon_check

scoreboard players set @s hc_beacon_size 0

execute @e[type=area_effect_cloud,c=1,tag=hc_test_beacon] ~ ~ ~ execute @s[tag=hc_beacon_found] ~ ~ ~ stats entity @s set AffectedBlocks @s hc_beacon_size
execute @e[type=area_effect_cloud,c=1,tag=hc_test_beacon] ~ ~ ~ execute @s[tag=hc_beacon_found] ~ ~ ~ scoreboard players set @s hc_beacon_size 0
execute @e[type=area_effect_cloud,c=1,tag=hc_test_beacon] ~ ~ ~ execute @s[tag=hc_beacon_found] ~ ~ ~ clone ~-1 ~-1 ~-1 ~1 ~-1 ~1 ~-1 ~-1 ~-1 filtered force minecraft:emerald_block
#tellraw @p ["",{"text":"1: "},{"score":{"name":"@e[type=area_effect_cloud,tag=hc_test_beacon,c=1]","objective":"hc_beacon_size"}}]
scoreboard players operation @s hc_beacon_size += @e[type=area_effect_cloud,c=1,tag=hc_test_beacon] hc_beacon_size
execute @e[type=area_effect_cloud,c=1,tag=hc_test_beacon] ~ ~ ~ execute @s[tag=hc_beacon_found] ~ ~ ~ clone ~-2 ~-2 ~-2 ~2 ~-2 ~2 ~-2 ~-2 ~-2 filtered force minecraft:emerald_block
#tellraw @p ["",{"text":"2: "},{"score":{"name":"@e[type=area_effect_cloud,tag=hc_test_beacon,c=1]","objective":"hc_beacon_size"}}]
scoreboard players operation @s hc_beacon_size += @e[type=area_effect_cloud,c=1,tag=hc_test_beacon] hc_beacon_size
execute @e[type=area_effect_cloud,c=1,tag=hc_test_beacon] ~ ~ ~ execute @s[tag=hc_beacon_found] ~ ~ ~ clone ~-3 ~-3 ~-3 ~3 ~-3 ~3 ~-3 ~-3 ~-3 filtered force minecraft:emerald_block
#tellraw @p ["",{"text":"3: "},{"score":{"name":"@e[type=area_effect_cloud,tag=hc_test_beacon,c=1]","objective":"hc_beacon_size"}}]
scoreboard players operation @s hc_beacon_size += @e[type=area_effect_cloud,c=1,tag=hc_test_beacon] hc_beacon_size
execute @e[type=area_effect_cloud,c=1,tag=hc_test_beacon] ~ ~ ~ execute @s[tag=hc_beacon_found] ~ ~ ~ clone ~-4 ~-4 ~-4 ~4 ~-4 ~4 ~-4 ~-4 ~-4 filtered force minecraft:emerald_block
#tellraw @p ["",{"text":"4: "},{"score":{"name":"@e[type=area_effect_cloud,tag=hc_test_beacon,c=1]","objective":"hc_beacon_size"}}]
scoreboard players operation @s hc_beacon_size += @e[type=area_effect_cloud,c=1,tag=hc_test_beacon] hc_beacon_size

advancement grant @s[score_hc_beacon_size_min=164] only hermitcraft:python/emerald_beacon

advancement revoke @s[score_hc_beacon_size=163] only hermitcraft:python/emerald_beacon_trigger

kill @e[type=area_effect_cloud,c=1,tag=hc_test_beacon]