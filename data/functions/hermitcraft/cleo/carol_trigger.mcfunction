#here is the solution if https://bugs.mojang.com/browse/MC-54972 is fixed.
#execute @s[score_hc_nametag_min=1] ~ ~ ~ scoreboard players tag @e[type=zombie,name=Carol,tag=!hc_processed,r=10] add hc_process
#execute @e[type=zombie,name=Carol,tag=hc_process,r=10] ~ ~ ~ advancement grant @a[r=10,score_hc_nametag_min=1] only hermitcraft:cleo/carol
#scoreboard players tag @e[type=zombie,name=Carol,tag=hc_process] add hc_processed 
#advancement revoke @s only hermitcraft:cleo/carol_trigger



#here is the solution for now

summon area_effect_cloud ~ ~ ~ {Tags:["hc_test_aec"],Particle:"take"}

stats entity @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] set AffectedItems @s hc_nametag
scoreboard players set @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_nametag 0
execute @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] ~ ~ ~ /clear @a[c=1,r=0] minecraft:name_tag 0 0

scoreboard players operation @s hc_nametag -= @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_nametag

execute @s[score_hc_nametag_min=1] ~ ~ ~ scoreboard players tag @e[type=zombie,name=Carol,tag=!hc_processed,r=10] add hc_process
execute @e[type=zombie,name=Carol,tag=hc_process,r=10] ~ ~ ~ execute @e[type=area_effect_cloud,r=10,tag=hc_test_aec] ~ ~ ~ advancement grant @a[r=10,score_hc_nametag_min=1] only hermitcraft:cleo/carol
scoreboard players tag @e[type=zombie,name=Carol,tag=hc_process] add hc_processed


scoreboard players operation @s hc_nametag = @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_nametag


advancement revoke @s only hermitcraft:cleo/carol_trigger

kill @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec]