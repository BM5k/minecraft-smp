scoreboard players tag @s[score_hc_blocks_placed_min=3] remove hc_su
scoreboard players tag @s[score_hc_blocks_placed_min=3] remove hc_su_sl
scoreboard players tag @s[score_hc_blocks_placed_min=3] remove hc_su_sl_su

scoreboard players tag @s[tag=hc_su_sl,score_hc_blocks_placed=1] add hc_su_sl_su

scoreboard players tag @s[tag=!hc_su] add hc_su


scoreboard players set @s hc_blocks_placed 0

advancement revoke @s only hermitcraft:mumbo/remixed_sugarcane_trigger