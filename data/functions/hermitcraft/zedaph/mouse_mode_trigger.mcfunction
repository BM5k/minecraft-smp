#initial detection
scoreboard players tag @s add hc_air
scoreboard players tag @s remove hc_air {Inventory:[{Slot:102b,id:"minecraft:elytra"}]}
execute @s ~ ~ ~ detect ~ ~-1 ~ air * scoreboard players tag @s add hc_air
execute @s ~ ~ ~ detect ~ ~1 ~ air * scoreboard players tag @s add hc_air

#minecarts and boats
scoreboard players tag @s[tag=!hc_air] add hc_air {RootVehicle:{Entity:{id:"minecraft:boat"}}}
scoreboard players tag @s[tag=!hc_air] add hc_air {RootVehicle:{Entity:{id:"minecraft:minecart"}}}

function hermitcraft:zedaph/mouse_mode_trigger_failsafe if @s[tag=!hc_air]

advancement revoke @s only hermitcraft:zedaph/mouse_mode_trigger