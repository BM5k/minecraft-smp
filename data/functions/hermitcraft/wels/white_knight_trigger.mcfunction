scoreboard players tag @s[tag=hc_whiteKnight] remove hc_whiteKnight
scoreboard players tag @s[tag=hc_whiteKnightArmor] remove hc_whiteKnightArmor

scoreboard players tag @s add hc_whiteKnight {RootVehicle:{Entity:{id:"minecraft:horse",Variant:0}}}
scoreboard players tag @s add hc_whiteKnight {RootVehicle:{Entity:{id:"minecraft:horse",Variant:256}}}

scoreboard players tag @s[tag=hc_whiteKnight] add hc_whiteKnightArmor {Inventory:[{Slot:100b,id:"minecraft:iron_boots"},{Slot:101b,id:"minecraft:iron_leggings"},{Slot:102b,id:"minecraft:iron_chestplate"},{Slot:103b,id:"minecraft:iron_helmet"}]}


advancement grant @s[tag=hc_whiteKnightArmor] only hermitcraft:wels/white_knight

advancement revoke @s[tag=!hc_whiteKnightArmor] only hermitcraft:wels/white_knight_trigger
