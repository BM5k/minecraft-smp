
summon area_effect_cloud ~ ~ ~ {Tags:["hc_test_aec"],Particle:"take"}

stats entity @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] set AffectedItems @s hc_nametag_3
scoreboard players set @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_nametag_3 0
execute @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] ~ ~ ~ /clear @a[c=1,r=0] minecraft:name_tag 0 0

scoreboard players operation @s hc_nametag_3 -= @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_nametag_3

execute @s[score_hc_nametag_3_min=1] ~ ~ ~ scoreboard players tag @e[type=sheep,name=Victoria,tag=!hc_processed,r=10] add hc_process
execute @e[type=sheep,name=Victoria,tag=hc_process,r=10] ~ ~ ~ advancement grant @a[r=10,score_hc_nametag_3_min=1] only hermitcraft:stress/victoria_the_sheep
scoreboard players tag @e[type=sheep,name=Victoria,tag=hc_process] add hc_processed


scoreboard players operation @s hc_nametag_3 = @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_nametag_3


advancement revoke @s only hermitcraft:stress/victoria_the_sheep_trigger

kill @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec]