execute @s ~ ~ ~ detect ~ ~ ~ beacon * scoreboard players tag @s add hc_beacon_found
#summon area_effect_cloud ~ ~ ~ {CustomName:"here",CustomNameVisible:1,Tags:["hc_test"],Particle:mobSpell,Duration:2000}


#move on X
teleport @s[tag=!hc_beacon_found,score_hc_beacon_pos_x=4] ~1 ~ ~
scoreboard players add @s[tag=!hc_beacon_found,score_hc_beacon_pos_x=4] hc_beacon_pos_x 1
execute @s[tag=!hc_beacon_found,score_hc_beacon_pos_x=4] ~ ~ ~ function hermitcraft:python/emerald_beacon_check

#move on y
#teleport @s[tag=!hc_beacon_found,score_hc_beacon_pos_x_min=5,score_hc_beacon_pos_y=4] ~-10 ~1 ~
#scoreboard players set @s[tag=!hc_beacon_found,score_hc_beacon_pos_x_min=5,score_hc_beacon_pos_y=4] hc_beacon_pos_x -5
#scoreboard players add @s[tag=!hc_beacon_found,score_hc_beacon_pos_y=4] hc_beacon_pos_y 1
#execute @s[tag=!hc_beacon_found,score_hc_beacon_pos_y=4] ~ ~ ~ function hermitcraft:python/emerald_beacon_check

execute @s[tag=!hc_beacon_found,score_hc_beacon_pos_x_min=5,score_hc_beacon_pos_y=4] ~ ~ ~ function hermitcraft:python/emerald_beacon_check_reset_x


#move on z
#teleport @s[tag=!hc_beacon_found,score_hc_beacon_pos_x_min=5,score_hc_beacon_pos_y_min=5,score_hc_beacon_pos_z=4] ~-10 ~-10 ~1
#scoreboard players set @s[tag=!hc_beacon_found,score_hc_beacon_pos_x_min=5,score_hc_beacon_pos_y_min=5,score_hc_beacon_pos_z=4] hc_beacon_pos_x -5
#scoreboard players set @s[tag=!hc_beacon_found,score_hc_beacon_pos_y_min=5,score_hc_beacon_pos_z=4] hc_beacon_pos_y -5
#scoreboard players add @s[tag=!hc_beacon_found,score_hc_beacon_pos_z=4] hc_beacon_pos_z 1
#execute @s[tag=!hc_beacon_found,score_hc_beacon_pos_z=4] ~ ~ ~ function hermitcraft:python/emerald_beacon_check

execute @s[tag=!hc_beacon_found,score_hc_beacon_pos_x_min=5,score_hc_beacon_pos_y_min=5,score_hc_beacon_pos_z=4] ~ ~ ~ function hermitcraft:python/emerald_beacon_check_reset_y

#tellraw @p ["",{"text":"X: "},{"score":{"name":"@s","objective":"hc_beacon_pos_x"}},{"text":" ... Y: "},{"score":{"name":"@s","objective":"hc_beacon_pos_y"}},{"text":" ... Z: "},{"score":{"name":"@s","objective":"hc_beacon_pos_z"}}]
