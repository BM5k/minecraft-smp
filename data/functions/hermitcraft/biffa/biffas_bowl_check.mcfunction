#scoreboard players tag @e[type=item,tag=!hc_bowl,r=5] add hc_bowl {Item:{id:bowl,Count:1b,tag:{display:{Name:"Biffa's Bowl"}}}}
execute @e[type=item,tag=!hc_processed,r=5] ~ ~ ~ detect ~ ~ ~ minecraft:water * scoreboard players tag @e[type=item,tag=!hc_processed,r=5] add hc_bowl {Item:{id:"minecraft:bowl",Count:1b,tag:{display:{Name:"Biffa's Bowl"}}}}

execute @e[type=item,tag=hc_bowl] ~ ~ ~ advancement grant @a[r=5] only hermitcraft:biffa/biffas_bowl

scoreboard players tag @e[type=item,tag=hc_bowl] add hc_processed
scoreboard players tag @e[type=item,tag=hc_bowl] remove hc_bowl


advancement revoke @s only hermitcraft:biffa/biffas_bowl_check
advancement revoke @s only hermitcraft:biffa/biffas_bowl_check2