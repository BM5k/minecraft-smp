#this one gets called when the player is above y=0, so if they have the hc_below64 Tag, they get the advancement.

scoreboard players tag @s[score_hc_death2_min=1,tag=hc_below64] remove hc_below64
scoreboard players set @s[score_hc_death2_min=1] hc_death2 0


advancement grant @s[tag=hc_below64] only hermitcraft:xisuma/embrace_the_void

advancement revoke @s[tag=!hc_below64] only hermitcraft:xisuma/embrace_the_void_trigger2

scoreboard players tag @s[tag=hc_below64] remove hc_below64