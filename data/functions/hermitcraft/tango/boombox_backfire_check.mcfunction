#grant advancement if died from explosion damage near an exploding Tnt

advancement grant @s[tag=hc_nearTnt,score_hc_tnt_death_min=1] only hermitcraft:tango/boombox_backfire
execute @s[tag=!hc_nearTnt] ~ ~ ~ advancement revoke @s only hermitcraft:tango/boombox_backfire_check
execute @s[score_hc_tnt_death=0] ~ ~ ~ advancement revoke @s only hermitcraft:tango/boombox_backfire_check
scoreboard players set @s[score_hc_tnt_death_min=1] hc_tnt_death 0
