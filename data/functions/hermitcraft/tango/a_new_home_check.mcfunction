
# Revoke the advancement check so it runs again
	advancement revoke @s only hermitcraft:tango/a_new_home_check


# Grant the advancement if player is near a shulker
	
	execute @e[type=shulker,tag=!_granted,r=16] ~ ~ ~ advancement grant @p[r=16] only hermitcraft:tango/a_new_home


# Give shulker the _granted tag so it doesn't grant the advancement to other people
	
	scoreboard players tag @e[type=shulker,tag=!_granted,r=16] add _granted