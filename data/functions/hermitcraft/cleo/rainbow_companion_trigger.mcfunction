
summon area_effect_cloud ~ ~ ~ {Tags:["hc_test_aec"],Particle:"take"}

stats entity @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] set AffectedItems @s hc_nametag_2
scoreboard players set @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_nametag_2 0
execute @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] ~ ~ ~ /clear @a[c=1,r=0] minecraft:name_tag 0 0

scoreboard players operation @s hc_nametag_2 -= @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_nametag_2

execute @s[score_hc_nametag_2_min=1] ~ ~ ~ scoreboard players tag @e[type=sheep,name=jeb_,tag=!hc_processed,r=10] add hc_process
execute @e[type=sheep,name=jeb_,tag=hc_process,r=10] ~ ~ ~ scoreboard players add @a[r=10,score_hc_nametag_2_min=1] hc_rainbow 1
execute @e[type=sheep,name=jeb_,tag=hc_process,r=10] ~ ~ ~ execute @e[type=area_effect_cloud,r=10,tag=hc_test_aec] ~ ~ ~ advancement grant @a[r=10,score_hc_nametag_2_min=1,score_hc_rainbow_min=1] only hermitcraft:cleo/rainbow_companion make_one_sheep
execute @e[type=sheep,name=jeb_,tag=hc_process,r=10] ~ ~ ~ execute @e[type=area_effect_cloud,r=10,tag=hc_test_aec] ~ ~ ~ advancement grant @a[r=10,score_hc_nametag_2_min=1,score_hc_rainbow_min=2] only hermitcraft:cleo/rainbow_companion make_two_sheep
scoreboard players tag @e[type=sheep,name=jeb_,tag=hc_process] add hc_processed
scoreboard players tag @e[type=sheep,name=jeb_,tag=hc_process] remove hc_process


scoreboard players operation @s hc_nametag_2 = @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_nametag_2


advancement revoke @s only hermitcraft:cleo/rainbow_companion_trigger

kill @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec]