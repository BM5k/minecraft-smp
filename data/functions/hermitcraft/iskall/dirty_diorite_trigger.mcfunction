scoreboard players tag @s[tag=hc_diorite] remove hc_diorite

scoreboard players tag @e[type=item,r=2,tag=!hc_diorite] add hc_diorite {Item:{id:"minecraft:stone",Damage:3s}}
scoreboard players tag @e[type=item,r=2,tag=!hc_diorite] add hc_diorite {Item:{id:"minecraft:stone",Damage:4s}}

execute @e[type=item,tag=hc_diorite] ~ ~ ~ scoreboard players tag @a[c=1,r=2] add hc_diorite

advancement revoke @s only hermitcraft:iskall/dirty_diorite_check

advancement revoke @s only hermitcraft:iskall/dirty_diorite_trigger