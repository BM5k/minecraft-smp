scoreboard players tag @s[tag=hc_diamond] remove hc_diamond

scoreboard players tag @e[type=item,r=2,tag=!hc_diamond] add hc_diamond {Item:{id:"minecraft:diamond"}}

execute @e[type=item,tag=hc_diamond] ~ ~ ~ scoreboard players tag @a[c=1,r=2] add hc_diamond

advancement revoke @s only hermitcraft:etho/diamonds_check

advancement revoke @s only hermitcraft:etho/diamonds_trigger