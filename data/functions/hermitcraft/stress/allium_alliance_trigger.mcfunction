#check for 100 allium flowers in the inventory

summon area_effect_cloud ~ ~ ~ {Tags:["hc_test_aec"],Particle:"take"}

#test for amount of flowers

stats entity @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] set AffectedItems @s hc_flowers
scoreboard players set @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_flowers 0
execute @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] ~ ~ ~ /clear @a[c=1,r=0] minecraft:red_flower 2 0

scoreboard players operation @s hc_flowers = @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_flowers

advancement grant @s[score_hc_flowers_min=100] only hermitcraft:stress/allium_alliance

advancement revoke @s[score_hc_flowers=99] only hermitcraft:stress/allium_alliance_trigger

kill @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec]