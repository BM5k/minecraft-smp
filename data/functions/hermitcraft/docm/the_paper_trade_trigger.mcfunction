#check for 1280 paper in the inventory

summon area_effect_cloud ~ ~ ~ {Tags:["hc_test_aec"],Particle:"take"}

#test for amount of writable books

stats entity @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] set AffectedItems @s hc_paper
scoreboard players set @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_paper 0
execute @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] ~ ~ ~ /clear @a[c=1,r=0] minecraft:paper 0 0

scoreboard players operation @s hc_paper = @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_paper


advancement grant @s[score_hc_paper_min=1280] only hermitcraft:docm/the_paper_trade

advancement revoke @s[score_hc_paper=1279] only hermitcraft:docm/the_paper_trade_trigger

kill @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec]