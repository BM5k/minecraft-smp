
summon area_effect_cloud ~ ~ ~ {Tags:["hc_test_aec"],Particle:"take"}

#fish
stats entity @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] set AffectedItems @s hc_fish
scoreboard players set @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_fish 0
execute @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] ~ ~ ~ /clear @a[c=1,r=0] minecraft:fish 0 0

scoreboard players operation @s hc_fish -= @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_fish


#salmon
stats entity @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] set AffectedItems @s hc_salmon
scoreboard players set @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_salmon 0
execute @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] ~ ~ ~ /clear @a[c=1,r=0] minecraft:fish 1 0

scoreboard players operation @s hc_salmon -= @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_salmon


#clownfish
stats entity @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] set AffectedItems @s hc_clownfish
scoreboard players set @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_clownfish 0
execute @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] ~ ~ ~ /clear @a[c=1,r=0] minecraft:fish 2 0

scoreboard players operation @s hc_clownfish -= @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_clownfish


#pufferfish
stats entity @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] set AffectedItems @s hc_puffer
scoreboard players set @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_puffer 0
execute @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] ~ ~ ~ /clear @a[c=1,r=0] minecraft:fish 3 0

scoreboard players operation @s hc_puffer -= @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_puffer

#tellraw @p ["",{"text":"fish: "},{"score":{"name":"@p","objective":"hc_fish"}},{"text":" - salmon: "},{"score":{"name":"@p","objective":"hc_salmon"}},{"text":" - clown: "},{"score":{"name":"@p","objective":"hc_clownfish"}},{"text":" - pufferfish: "},{"score":{"name":"@p","objective":"hc_puffer"}}]


advancement grant @s[score_hc_fished_2_min=1,score_hc_fish=-1] only hermitcraft:cleo/all_the_fish get_fish
advancement grant @s[score_hc_fished_2_min=1,score_hc_salmon=-1] only hermitcraft:cleo/all_the_fish get_salmon
advancement grant @s[score_hc_fished_2_min=1,score_hc_clownfish=-1] only hermitcraft:cleo/all_the_fish get_clownfish
advancement grant @s[score_hc_fished_2_min=1,score_hc_puffer=-1] only hermitcraft:cleo/all_the_fish get_pufferfish


scoreboard players operation @s hc_fish = @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_fish
scoreboard players operation @s hc_salmon = @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_salmon
scoreboard players operation @s hc_clownfish = @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_clownfish
scoreboard players operation @s hc_puffer = @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_puffer


scoreboard players set @s[score_hc_fished_2_min=1] hc_fished_2 0

advancement revoke @s only hermitcraft:cleo/all_the_fish_trigger

kill @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec]