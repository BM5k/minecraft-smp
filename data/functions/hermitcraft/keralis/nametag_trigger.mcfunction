
summon area_effect_cloud ~ ~ ~ {Tags:["hc_test_aec"],Particle:"take"}

stats entity @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] set AffectedItems @s hc_nametag_4
scoreboard players set @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_nametag_4 0
execute @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] ~ ~ ~ /clear @a[c=1,r=0] minecraft:name_tag 0 0

scoreboard players operation @s hc_nametag_4 -= @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_nametag_4

execute @s[score_hc_nametag_4_min=1] ~ ~ ~ scoreboard players tag @e[type=horse,name=Nametag,tag=!hc_processed,r=10] add hc_process
execute @e[type=horse,name=Nametag,tag=hc_process,r=10] ~ ~ ~ execute @e[type=area_effect_cloud,r=10,tag=hc_test_aec] ~ ~ ~ advancement grant @a[r=1,score_hc_nametag_4_min=1,c=1] only hermitcraft:keralis/nametag
scoreboard players tag @e[type=horse,name=Nametag,tag=hc_process] add hc_processed
scoreboard players tag @e[type=horse,name=Nametag,tag=hc_process] remove hc_process


scoreboard players operation @s hc_nametag_4 = @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_nametag_4


advancement revoke @s only hermitcraft:keralis/nametag_trigger

kill @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec]