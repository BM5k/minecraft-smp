
scoreboard players tag @e[type=item,tag=!hc_pris_processed,r=10] add hc_prismarine1 {Item:{id:"minecraft:prismarine",Damage:0s}}
scoreboard players tag @e[type=item,tag=!hc_pris_processed,r=10] add hc_prismarine2 {Item:{id:"minecraft:prismarine",Damage:1s}}
scoreboard players tag @e[type=item,tag=!hc_pris_processed,r=10] add hc_prismarine3 {Item:{id:"minecraft:prismarine",Damage:2s}}

execute @e[type=item,tag=hc_prismarine1,r=10] ~ ~ ~ scoreboard players add @a[score_hc_prismarine_min=1,r=10] hc_prismarine1 1
execute @e[type=item,tag=hc_prismarine2,r=10] ~ ~ ~ scoreboard players add @a[score_hc_prismarine_min=1,r=10] hc_prismarine2 1
execute @e[type=item,tag=hc_prismarine3,r=10] ~ ~ ~ scoreboard players add @a[score_hc_prismarine_min=1,r=10] hc_prismarine3 1

scoreboard players tag @e[type=item,tag=hc_prismarine1,r=10] add hc_pris_processed
scoreboard players tag @e[type=item,tag=hc_prismarine2,r=10] add hc_pris_processed
scoreboard players tag @e[type=item,tag=hc_prismarine3,r=10] add hc_pris_processed
scoreboard players tag @e[type=item,tag=hc_prismarine1,r=10] remove hc_prismarine1
scoreboard players tag @e[type=item,tag=hc_prismarine2,r=10] remove hc_prismarine2
scoreboard players tag @e[type=item,tag=hc_prismarine3,r=10] remove hc_prismarine3




advancement grant @s[score_hc_prismarine1_min=1,score_hc_prismarine2_min=1,score_hc_prismarine3_min=1,score_hc_sponge_min=1,score_hc_gold_block_min=1,score_hc_sea_lantern_min=1] only hermitcraft:xbcrafted/make_it_home


advancement revoke @s only hermitcraft:xbcrafted/make_it_home_trigger

scoreboard players set @s[score_hc_prismarine_min=1] hc_prismarine 0
