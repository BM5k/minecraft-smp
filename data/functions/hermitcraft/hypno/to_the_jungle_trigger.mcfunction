#check for 30 jungle saplings in the inventory

summon area_effect_cloud ~ ~ ~ {Tags:["hc_test_aec"],Particle:"take"}


stats entity @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] set AffectedItems @s hc_sapling
scoreboard players set @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_sapling 0
execute @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] ~ ~ ~ /clear @a[c=1,r=0] minecraft:sapling 3 0

scoreboard players operation @s hc_sapling = @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_sapling

advancement grant @s[score_hc_sapling_min=30] only hermitcraft:hypno/to_the_jungle

advancement revoke @s[score_hc_sapling=29] only hermitcraft:hypno/to_the_jungle_trigger

kill @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec]