#check for one less writable_book and one more written_book

summon area_effect_cloud ~ ~ ~ {Tags:["hc_test_aec"],Particle:"take"}

#test for amount of writable books

stats entity @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] set AffectedItems @s hc_writablebook
scoreboard players set @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_writablebook 0
execute @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] ~ ~ ~ /clear @a[c=1,r=0] minecraft:writable_book 0 0

scoreboard players operation @s hc_writablebook -= @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_writablebook

#test for amount of written books

stats entity @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] set AffectedItems @s hc_writtenbook
scoreboard players set @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_writtenbook 0
execute @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] ~ ~ ~ /clear @a[c=1,r=0] minecraft:written_book 0 0

scoreboard players operation @s hc_writtenbook -= @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_writtenbook

#tellraw @p ["",{"text":"writable: "},{"score":{"name":"@p","objective":"hc_writablebook"}},{"text":" - written: "},{"score":{"name":"@p","objective":"hc_writtenbook"}}]
#say @s[score_hc_writtenbook_min=-1,score_hc_writtenbook=-1,score_hc_writablebook_min=1,score_hc_writablebook=1]

advancement grant @s[score_hc_writtenbook_min=-1,score_hc_writtenbook=-1,score_hc_writablebook_min=1,score_hc_writablebook=1] only hermitcraft:joehills/poet


scoreboard players operation @s hc_writablebook = @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_writablebook
scoreboard players operation @s hc_writtenbook = @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_writtenbook


advancement revoke @s only hermitcraft:joehills/poet_trigger

kill @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec]