#initiate all the scoreboards needed in here

#general
gamerule commandBlockOutput false
gamerule logAdminCommands false
scoreboard objectives add hc_log stat.mineBlock.minecraft.log
scoreboard players add hc_log 0


#iJevin
scoreboard objectives add hc_slimes_killed stat.killEntity.Slime
scoreboard players add @s hc_slimes_killed 0
scoreboard objectives add hc_wool_in_inv dummy
scoreboard objectives add hc_emerald_ore stat.mineBlock.minecraft.emerald_ore
scoreboard players add @s hc_emerald_ore 0


#Tango
scoreboard objectives add hc_golems_killed dummy
scoreboard objectives add hc_tnt_death stat.deaths
scoreboard players add @s hc_tnt_death 0

#Mumbo
scoreboard objectives add hc_blocks_placed dummy
scoreboard objectives add hc_death stat.deaths
scoreboard players add @s hc_death 0
scoreboard objectives add hc_deathTimer dummy
scoreboard players add @s hc_deathTimer 0

#JoeHills
scoreboard objectives add hc_paintings stat.useItem.minecraft.painting
scoreboard players set @s hc_paintings 0
scoreboard objectives add hc_writablebook dummy
scoreboard objectives add hc_writtenbook dummy
scoreboard objectives add hc_harvest_wheat dummy
scoreboard players add @s hc_harvest_wheat 0

#DocM
scoreboard objectives add hc_paper dummy
scoreboard objectives add hc_cured dummy

#Rendog
scoreboard objectives add hc_oak dummy
scoreboard players add @s hc_oak 0

#Cubfan
scoreboard objectives add hc_birch dummy
scoreboard players add @s hc_birch 0
scoreboard objectives add hc_sand stat.mineBlock.minecraft.sand
scoreboard players add @s hc_sand 0

#Stress
scoreboard objectives add hc_flowers dummy
scoreboard objectives add hc_nametag_3 dummy

#Bdubs
scoreboard objectives add hc_fished stat.fishCaught
scoreboard players add @s hc_fished 0
scoreboard objectives add hc_pick_emerald stat.pickup.minecraft.emerald
scoreboard players add @s hc_pick_emerald 0

#Cleo
scoreboard objectives add hc_fished_2 stat.fishCaught
scoreboard players add @s hc_fished_2 0
scoreboard objectives add hc_fish dummy
scoreboard objectives add hc_salmon dummy
scoreboard objectives add hc_clownfish dummy
scoreboard objectives add hc_puffer dummy
#scoreboard objectives add hc_nametag stat.useItem.minecraft.name_tag
scoreboard objectives add hc_nametag dummy
scoreboard objectives add hc_nametag_2 dummy
scoreboard objectives add hc_rainbow dummy
scoreboard players add @s hc_rainbow 0

#TFC
scoreboard objectives add hc_obsidian stat.mineBlock.minecraft.obsidian
scoreboard players add @s hc_obsidian 0
scoreboard objectives add hc_diamond_ore stat.mineBlock.minecraft.diamond_ore
scoreboard players add @s hc_diamond_ore 0
scoreboard objectives add hc_eat_chicken dummy

#Welsknight
scoreboard objectives add hc_stone stat.mineBlock.minecraft.stone
scoreboard players add @s hc_stone 0
scoreboard objectives add hc_endermen stat.killEntity.Enderman
scoreboard players add @s hc_endermen 0

#Xisuma
scoreboard objectives add hc_death2 stat.deaths
scoreboard players add @s hc_death2 0

#Biffa
scoreboard objectives add hc_bowl dummy

#Hypno
scoreboard objectives add hc_sapling dummy
scoreboard objectives add hc_stone_brick dummy
scoreboard objectives add hc_emerald_block dummy

#Impulse
scoreboard objectives add hc_witch stat.killEntity.Witch
scoreboard players add @s hc_witch 0
scoreboard objectives add hc_guardian stat.killEntity.Guardian
scoreboard players add @s hc_guardian 0

#Keralis
scoreboard objectives add hc_nametag_4 dummy
scoreboard objectives add hc_booshes dummy
scoreboard objectives add hc_skeleton stat.killEntity.Skeleton
scoreboard players add @s hc_skeleton 0

#Python
scoreboard objectives add hc_beacon dummy
scoreboard objectives add hc_beacon_size dummy
scoreboard objectives add hc_beacon_pos_x dummy
scoreboard objectives add hc_beacon_pos_y dummy
scoreboard objectives add hc_beacon_pos_z dummy
scoreboard objectives add hc_trade stat.tradedWithVillager
scoreboard players add @s hc_trade 0

#VintageBeef
scoreboard objectives add hc_fall stat.fallOneCm
scoreboard players add @s hc_fall 0
scoreboard objectives add hc_death3 stat.deaths
scoreboard players set hc_death3 0
scoreboard objectives add hc_raw_beef dummy

#xBCrafted
scoreboard objectives add hc_prismarine stat.mineBlock.minecraft.prismarine
scoreboard objectives add hc_prismarine1 dummy
scoreboard objectives add hc_prismarine2 dummy
scoreboard objectives add hc_prismarine3 dummy
scoreboard objectives add hc_sea_lantern stat.mineBlock.minecraft.sea_lantern
scoreboard objectives add hc_gold_block stat.mineBlock.minecraft.gold_block
scoreboard objectives add hc_sponge stat.mineBlock.minecraft.sponge

scoreboard players add @s hc_prismarine 0
scoreboard players add @s hc_sea_lantern 0
scoreboard players add @s hc_gold_block 0
scoreboard players add @s hc_sponge 0

scoreboard objectives add hc_nametag_5 dummy

#Etho
scoreboard objectives add hc_dragon_alive dummy

#Scar
scoreboard objectives add hc_aviateOneCm stat.aviateOneCm
scoreboard players add @s hc_aviateOneCm 1

#Jessassin
scoreboard objectives add hc_glass dummy
scoreboard objectives add hc_use_sponge stat.useItem.minecraft.sponge
scoreboard players add @s hc_use_sponge 0

#Zedaph
scoreboard objectives add hc_flying_sheep stat.aviateOneCm
scoreboard players set @s hc_flying_sheep 0
scoreboard objectives add hc_success dummy

scoreboard objectives add hc_death4 stat.deaths
scoreboard players set hc_death4 0

#welcome message

tellraw @s ["",{"text":"\nWelcome to the ","color":"aqua","italic":true},{"text":"Hermitcraft Challenge\n","color":"aqua","italic":true,"bold":true,"clickEvent":{"action":"open_url","value":"http://hermitcraft.com"},"hoverEvent":{"action":"show_text","value":"Click here for Hermitcraft Website"}},{"text":"This challenge includes a lot of custom advancements that all represent their hermit in some way for you to try to complete.","italic":true,"color":"dark_aqua"}]
tellraw @s ["",{"text":"   » Idea: ","italic":true},{"text":"Xisuma","color":"gold","italic":true,"underline":true,"clickEvent":{"action":"open_url","value":"http://xisumavoid.com"},"hoverEvent":{"action":"show_text","value":"Click here for Website"}},{"text":" - "},{"text":"Twitter","color":"blue","italic":true,"underline":true,"clickEvent":{"action":"open_url","value":"http://twitter.com/xisumavoid"},"hoverEvent":{"action":"show_text","value":"Click here for Twitter"}},{"text":" - "},{"text":"Youtube","color":"dark_red","italic":true,"underline":true,"clickEvent":{"action":"open_url","value":"https://www.youtube.com/user/xisumavoid"},"hoverEvent":{"action":"show_text","value":"Click here for Youtube"}}]
tellraw @s ["",{"text":"   » Main Creator: ","italic":true},{"text":"Plagiatus","color":"gold","italic":true,"underline":true,"clickEvent":{"action":"open_url","value":"http://plagiatus.net"},"hoverEvent":{"action":"show_text","value":"Click here for Website"}},{"text":" - "},{"text":"Twitter","color":"blue","italic":true,"underline":true,"clickEvent":{"action":"open_url","value":"http://twitter.com/realplagiatus"},"hoverEvent":{"action":"show_text","value":"Click here for Twitter"}},{"text":" - "},{"text":"Youtube","color":"dark_red","italic":true,"underline":true,"clickEvent":{"action":"open_url","value":"https://youtube.com/therealplagiatus"},"hoverEvent":{"action":"show_text","value":"Click here for Youtube"}}]
tellraw @s ["",{"text":"   » Creator: ","italic":true},{"text":"Code__","color":"gold","italic":true,"underline":true,"clickEvent":{"action":"open_url","value":"http://reddit.com/user/Code__"},"hoverEvent":{"action":"show_text","value":"Click here for Reddit profile"}}]
tellraw @s ["",{"text":"   » Creator: ","italic":true},{"text":"destruc7i0n","color":"gold","italic":true,"underline":true,"clickEvent":{"action":"open_url","value":"http://thedestruc7i0n.ca"},"hoverEvent":{"action":"show_text","value":"Click here for Website"}},{"text":" - "},{"text":"Twitter","color":"blue","italic":true,"underline":true,"clickEvent":{"action":"open_url","value":"http://twitter.com/TheDestruc7i0n"},"hoverEvent":{"action":"show_text","value":"Click here for Twitter"}},{"text":" - "},{"text":"Youtube","color":"dark_red","italic":true,"underline":true,"clickEvent":{"action":"open_url","value":"https://youtube.com/c/TheDestruc7i0n"},"hoverEvent":{"action":"show_text","value":"Click here for Youtube"}}]
tellraw @s ["",{"text":"   » Creator: ","italic":true},{"text":"Misterx7772","color":"gold","italic":true,"underline":true},{"text":" - "},{"text":"Twitter","color":"blue","italic":true,"underline":true,"clickEvent":{"action":"open_url","value":"http://twitter.com/Misterx7772"},"hoverEvent":{"action":"show_text","value":"Click here for Twitter"}}]


#failsave_ticks
advancement revoke @s only hermitcraft:joehills/field_of_dreams_trigger
advancement revoke @s only hermitcraft:mumbo/deathmachine_trigger
advancement revoke @s only hermitcraft:vintage/beefs_knees_trigger
advancement revoke @s only hermitcraft:rendog/a_true_logfella_trigger
advancement revoke @s only hermitcraft:cubfan/birchfella_trigger
advancement revoke @s only hermitcraft:zedaph/top_fun_trigger
advancement revoke @s only hermitcraft:scar/5k_club_trigger

#failsave_every_other_trigger
advancement revoke @s only hermitcraft:bdubs/	the_old_school_way_trigger
advancement revoke @s only hermitcraft:biffa/biffas_bowl_check
advancement revoke @s only hermitcraft:biffa/biffas_bowl_check2
advancement revoke @s only hermitcraft:cleo/all_the_fish_trigger
advancement revoke @s only hermitcraft:cleo/carol_trigger
advancement revoke @s only hermitcraft:cleo/rainbow_companion_trigger
advancement revoke @s only hermitcraft:cubfan/desert_destroyer_trigger
advancement revoke @s only hermitcraft:cubfan/volcanic_safety_trigger
advancement revoke @s only hermitcraft:docm/doctor_undead_trigger
advancement revoke @s only hermitcraft:docm/the_paper_trade_trigger
advancement revoke @s only hermitcraft:etho/diamonds_check
advancement revoke @s only hermitcraft:etho/diamonds_trigger
advancement revoke @s only hermitcraft:etho/dragon_slayer_check
advancement revoke @s only hermitcraft:etho/dragon_slayer_trigger
advancement revoke @s only hermitcraft:false/jellyfish_crown_trigger
advancement revoke @s only hermitcraft:hypno/build_a_temple_trigger
advancement revoke @s only hermitcraft:hypno/make_it_green_trigger
advancement revoke @s only hermitcraft:hypno/to_the_jungle_trigger
advancement revoke @s only hermitcraft:ijevin/king_of_emeralds_trigger
advancement revoke @s only hermitcraft:ijevin/pink_death_trigger
advancement revoke @s only hermitcraft:ijevin/you_are_what_you_kill_trigger
advancement revoke @s only hermitcraft:impulse/impulse_all_trigger
advancement revoke @s only hermitcraft:iskall/dirty_diorite_trigger
advancement revoke @s only hermitcraft:iskall/dirty_diorite_check
advancement revoke @s only hermitcraft:jessassin/build_a_dome_trigger
advancement revoke @s only hermitcraft:jessassin/drain_it_trigger
advancement revoke @s only hermitcraft:joehills/artist_trigger
advancement revoke @s only hermitcraft:joehills/poet_trigger
advancement revoke @s only hermitcraft:keralis/booshes_trigger
advancement revoke @s only hermitcraft:keralis/bow_baddies_trigger
advancement revoke @s only hermitcraft:keralis/nametag_trigger
advancement revoke @s only hermitcraft:mumbo/remixed_bricks_trigger
advancement revoke @s only hermitcraft:mumbo/remixed_everything_trigger
advancement revoke @s only hermitcraft:mumbo/remixed_slab_trigger
advancement revoke @s only hermitcraft:mumbo/remixed_sugarcane_trigger
advancement revoke @s only hermitcraft:python/emerald_beacon_trigger
advancement revoke @s only hermitcraft:python/master_trader_trigger
advancement revoke @s only hermitcraft:python/penta_beacon_trigger
advancement revoke @s only hermitcraft:stress/allium_alliance_trigger
advancement revoke @s only hermitcraft:stress/victoria_the_sheep_trigger
advancement revoke @s only hermitcraft:tango/a_new_home_check
advancement revoke @s only hermitcraft:tango/boombox_backfire_check
advancement revoke @s only hermitcraft:tango/boombox_backfire_trigger
advancement revoke @s only hermitcraft:tango/time_to_farm_trigger
advancement revoke @s only hermitcraft:tfc/cluck_cluck_trigger
advancement revoke @s only hermitcraft:tfc/lord_of_the_grind_trigger
advancement revoke @s only hermitcraft:tfc/mining_king_trigger
advancement revoke @s only hermitcraft:vintage/beef_in_space_trigger
advancement revoke @s only hermitcraft:vintage/beef_in_space_trigger2
advancement revoke @s only hermitcraft:vintage/raw_beef_trigger
advancement revoke @s only hermitcraft:wels/dwarven_mine_trigger
advancement revoke @s only hermitcraft:wels/ender_ender_trigger
advancement revoke @s only hermitcraft:wels/white_knight_trigger
advancement revoke @s only hermitcraft:xbcrafted/make_it_home_trigger
advancement revoke @s only hermitcraft:xbcrafted/wooloo_trigger
advancement revoke @s only hermitcraft:xisuma/embrace_the_void_trigger
advancement revoke @s only hermitcraft:xisuma/embrace_the_void_trigger2
advancement revoke @s only hermitcraft:zedaph/mouse_mode_trigger
advancement revoke @s only hermitcraft:zedaph/naked_boom_trigger
