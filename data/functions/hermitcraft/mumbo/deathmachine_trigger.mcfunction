#how it works:
#we add 400 to the players hc_deathTimer objective every time the player dies, while removing one from it every tick. this will basically grant the player 20 seconds until the added points are gone. since a good death machine is able to kill you faster than once every 10 seconds (the best one can do around 5 seconds), you should end up with ~200 points gain  after every death in the deathmachine.
#to achieve the advancement the player needs to get a score of 3600, aka 18 deaths in quick succession. If you manage to stay below 10 seoconds for every death, it should take around 3 minutes on the death machine.
#we can obviously change that number quite easily

scoreboard players remove @s[score_hc_deathTimer_min=1] hc_deathTimer 1
scoreboard players add @s[score_hc_death_min=1] hc_deathTimer 400
scoreboard players remove @s[score_hc_death_min=1] hc_death 1


advancement grant @s[score_hc_deathTimer_min=3600] only hermitcraft:mumbo/deathmachine
advancement revoke @s[score_hc_deathTimer=3599] only hermitcraft:mumbo/deathmachine_trigger