#check for different paintings

execute @s[score_hc_paintings_min=1] ~ ~ ~ scoreboard players tag @e[type=painting,c=1,r=7,tag=!hc_checked] add hc_check

scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_kebab {Motive:"Kebab"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_aztec {Motive:"Aztec"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_alban {Motive:"Alban"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_aztec2 {Motive:"Aztec2"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_bomb {Motive:"Bomb"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_plant {Motive:"Plant"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_wasteland {Motive:"Wasteland"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_wanderer {Motive:"Wanderer"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_graham {Motive:"Graham"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_pool {Motive:"Pool"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_courbet {Motive:"Courbet"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_sunset {Motive:"Sunset"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_sea {Motive:"Sea"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_creebet {Motive:"Creebet"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_match {Motive:"Match"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_bust {Motive:"Bust"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_stage {Motive:"Stage"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_void {Motive:"Void"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_skullandroses {Motive:"SkullAndRoses"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_wither {Motive:"Wither"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_fighters {Motive:"Fighters"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_skeleton {Motive:"Skeleton"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_donkeykong {Motive:"DonkeyKong"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_pointer {Motive:"Pointer"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_pigscene {Motive:"Pigscene"}
scoreboard players tag @e[type=painting,tag=!hc_checked,r=7] add hc_burningskull {Motive:"BurningSkull"}

execute @e[type=painting,tag=hc_kebab] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_kebab
execute @e[type=painting,tag=hc_aztec] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_aztec
execute @e[type=painting,tag=hc_alban] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_alban
execute @e[type=painting,tag=hc_aztec2] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_aztec2
execute @e[type=painting,tag=hc_bomb] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_bomb
execute @e[type=painting,tag=hc_plant] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_plant
execute @e[type=painting,tag=hc_wasteland] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_wasteland
execute @e[type=painting,tag=hc_wanderer] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_wanderer
execute @e[type=painting,tag=hc_graham] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_graham
execute @e[type=painting,tag=hc_pool] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_pool
execute @e[type=painting,tag=hc_courbet] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_courbet
execute @e[type=painting,tag=hc_sunset] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_sunset
execute @e[type=painting,tag=hc_sea] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_sea
execute @e[type=painting,tag=hc_creebet] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_creebet
execute @e[type=painting,tag=hc_match] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_match
execute @e[type=painting,tag=hc_bust] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_bust
execute @e[type=painting,tag=hc_stage] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_stage
execute @e[type=painting,tag=hc_void] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_void
execute @e[type=painting,tag=hc_skullandroses] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_skullandroses
execute @e[type=painting,tag=hc_wither] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_wither
execute @e[type=painting,tag=hc_fighters] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_fighters
execute @e[type=painting,tag=hc_skeleton] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_skeleton
execute @e[type=painting,tag=hc_donkeykong] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_donkeykong
execute @e[type=painting,tag=hc_pointer] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_pointer
execute @e[type=painting,tag=hc_pigscene] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_pigscene
execute @e[type=painting,tag=hc_burningskull] ~ ~ ~ advancement grant @a[r=7,score_hc_paintings_min=1] only hermitcraft:joehills/artist placed_burningskull

#reset and block paintings so they won't execute next time

scoreboard players tag @e[type=painting,tag=!hc_checked] add hc_checked {Tags:["hc_check"]}

scoreboard players tag @e[type=painting,tag=hc_kebab] remove hc_kebab
scoreboard players tag @e[type=painting,tag=hc_aztec] remove hc_aztec
scoreboard players tag @e[type=painting,tag=hc_alban] remove hc_alban
scoreboard players tag @e[type=painting,tag=hc_aztec2] remove hc_aztec2
scoreboard players tag @e[type=painting,tag=hc_bomb] remove hc_bomb
scoreboard players tag @e[type=painting,tag=hc_plant] remove hc_plant
scoreboard players tag @e[type=painting,tag=hc_wasteland] remove hc_wasteland
scoreboard players tag @e[type=painting,tag=hc_wanderer] remove hc_wanderer
scoreboard players tag @e[type=painting,tag=hc_graham] remove hc_graham
scoreboard players tag @e[type=painting,tag=hc_pool] remove hc_pool
scoreboard players tag @e[type=painting,tag=hc_courbet] remove hc_courbet
scoreboard players tag @e[type=painting,tag=hc_sunset] remove hc_sunset
scoreboard players tag @e[type=painting,tag=hc_sea] remove hc_sea
scoreboard players tag @e[type=painting,tag=hc_creebet] remove hc_creebet
scoreboard players tag @e[type=painting,tag=hc_match] remove hc_match
scoreboard players tag @e[type=painting,tag=hc_bust] remove hc_bust
scoreboard players tag @e[type=painting,tag=hc_stage] remove hc_stage
scoreboard players tag @e[type=painting,tag=hc_void] remove hc_void
scoreboard players tag @e[type=painting,tag=hc_skullandroses] remove hc_skullandroses
scoreboard players tag @e[type=painting,tag=hc_wither] remove hc_wither
scoreboard players tag @e[type=painting,tag=hc_fighters] remove hc_fighters
scoreboard players tag @e[type=painting,tag=hc_skeleton] remove hc_skeleton
scoreboard players tag @e[type=painting,tag=hc_donkeykong] remove hc_donkeykong
scoreboard players tag @e[type=painting,tag=hc_pointer] remove hc_pointer
scoreboard players tag @e[type=painting,tag=hc_pigscene] remove hc_pigscene
scoreboard players tag @e[type=painting,tag=hc_burningskull] remove hc_burningskull


scoreboard players set @s[score_hc_paintings_min=1] hc_paintings 0

advancement revoke @s only hermitcraft:joehills/artist_trigger