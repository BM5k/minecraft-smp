scoreboard players tag @e[type=sheep,r=15,tag=hc_leashed] remove hc_leashed

scoreboard players tag @e[type=sheep,r=15] add hc_leashed {Leashed:1b}

#summon area_effect_cloud ~ ~ ~ {Tags:["hc_test_aec"],Particle:"take"}
#stats entity @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] set SuccessCount @s hc_success
#scoreboard players set @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_success 0
#execute @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] ~ ~ ~ /testfor @e[type=sheep,tag=hc_leashed,r=20]

#scoreboard players operation @s hc_success = @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_success

scoreboard players set @s hc_success 0
execute @e[type=sheep,tag=hc_leashed,r=15] ~ ~ ~ detect ~ ~-1 ~ air * scoreboard players set @a[r=15] hc_success 1

advancement grant @s[score_hc_flying_sheep_min=2000,score_hc_success_min=1] only hermitcraft:zedaph/top_fun
advancement revoke @s[score_hc_flying_sheep=1999] only hermitcraft:zedaph/top_fun_trigger
advancement revoke @s[score_hc_success=0] only hermitcraft:zedaph/top_fun_trigger

scoreboard players set @s[score_hc_flying_sheep_min=1] hc_flying_sheep 0

kill @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec]

