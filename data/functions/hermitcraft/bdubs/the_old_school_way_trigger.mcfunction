scoreboard players tag @s[tag=hc_inBoat] remove hc_inBoat
scoreboard players tag @s add hc_inBoat {RootVehicle:{Entity:{id:"minecraft:boat"}}}

advancement grant @s[tag=hc_inBoat,score_hc_fished_min=1] only hermitcraft:bdubs/the_old_school_way

advancement revoke @s[tag=!hc_inBoat] only hermitcraft:bdubs/the_old_school_way_trigger
advancement revoke @s[score_hc_fished=0] only hermitcraft:bdubs/the_old_school_way_trigger

scoreboard players set @s[score_hc_fished_min=1] hc_fished 0