scoreboard players add @s hc_beacon 1

advancement grant @s[score_hc_beacon_min=1] only hermitcraft:python/penta_beacon beacon_one
advancement grant @s[score_hc_beacon_min=2] only hermitcraft:python/penta_beacon beacon_two
advancement grant @s[score_hc_beacon_min=3] only hermitcraft:python/penta_beacon beacon_three
advancement grant @s[score_hc_beacon_min=4] only hermitcraft:python/penta_beacon beacon_four
advancement grant @s[score_hc_beacon_min=5] only hermitcraft:python/penta_beacon beacon_five


advancement revoke @s[score_hc_beacon=4] only hermitcraft:python/penta_beacon_trigger