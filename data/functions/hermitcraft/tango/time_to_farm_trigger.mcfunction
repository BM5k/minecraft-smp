#if (hc_golems_killed = 10) grant advancement

scoreboard players add @s hc_golems_killed 1
advancement grant @s[score_hc_golems_killed=10,score_hc_golems_killed_min=10] only hermitcraft:tango/time_to_farm
advancement revoke @s[score_hc_golems_killed=9] only hermitcraft:tango/time_to_farm_trigger