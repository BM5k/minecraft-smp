scoreboard players tag @a add hc_hasArmor {Inventory:[{Slot:100b}]}
scoreboard players tag @a add hc_hasArmor {Inventory:[{Slot:101b}]}
scoreboard players tag @a add hc_hasArmor {Inventory:[{Slot:102b}]}
scoreboard players tag @a add hc_hasArmor {Inventory:[{Slot:103b}]}

advancement grant @s[tag=!hc_hasArmor,score_hc_death4=0] only hermitcraft:zedaph/naked_boom
advancement revoke @s[tag=hc_hasArmor] only hermitcraft:zedaph/naked_boom_trigger
advancement revoke @s[score_hc_death4_min=1] only hermitcraft:zedaph/naked_boom_trigger


scoreboard players set @s[score_hc_death4_min=1] hc_death4 0
scoreboard players tag @s[tag=hc_hasArmor] remove hc_hasArmor