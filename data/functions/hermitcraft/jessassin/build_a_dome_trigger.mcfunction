execute @s ~ ~ ~ detect ~ ~ ~ water * scoreboard players tag @s add hc_inWater 

scoreboard players add @s[tag=hc_inWater] hc_glass 1

advancement grant @s[score_hc_glass_min=64] only hermitcraft:jessassin/build_a_dome
advancement revoke @s[score_hc_glass=63] only hermitcraft:jessassin/build_a_dome_trigger

scoreboard players tag @s[tag=hc_inWater] remove hc_inWater 