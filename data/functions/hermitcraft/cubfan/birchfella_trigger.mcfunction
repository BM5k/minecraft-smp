scoreboard players tag @e[type=item,tag=!hc_log_processed,r=10] add hc_birch {Item:{id:"minecraft:log",Damage:2s}}
execute @e[type=item,tag=hc_birch,r=10] ~ ~ ~ scoreboard players add @a[score_hc_log_min=1,r=10] hc_birch 1
scoreboard players tag @e[type=item,tag=hc_birch] add hc_log_processed
scoreboard players tag @e[type=item,tag=hc_birch] remove hc_birch
scoreboard players set @s[score_hc_log_min=1] hc_log 0

advancement grant @s[score_hc_birch_min=500] only hermitcraft:cubfan/birchfella

advancement revoke @s[score_hc_birch=499] only hermitcraft:cubfan/birchfella_trigger