#check for 1280 paper in the inventory

scoreboard players tag @e[type=item,tag=!hc_log_processed,r=10] add hc_oak {Item:{id:"minecraft:log",Damage:0s}}
execute @e[type=item,tag=hc_oak] ~ ~ ~ scoreboard players add @a[score_hc_log_min=1,r=10] hc_oak 1
scoreboard players tag @e[type=item,tag=hc_oak] add hc_log_processed
scoreboard players tag @e[type=item,tag=hc_oak] remove hc_oak
scoreboard players set @s[score_hc_log_min=1] hc_log 0

advancement grant @s[score_hc_oak_min=500] only hermitcraft:rendog/a_true_logfella

advancement revoke @s[score_hc_oak=499] only hermitcraft:rendog/a_true_logfella_trigger