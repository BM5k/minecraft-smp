advancement revoke @s only hermitcraft:etho/dragon_slayer_check

scoreboard players tag @s[tag=!hc_end] add hc_end {Dimension:1}
scoreboard players tag @s[tag=hc_end] remove hc_end {Dimension:0}
scoreboard players tag @s[tag=hc_end] remove hc_end {Dimension:-1}

scoreboard players tag @s[tag=!hc_end] remove hc_onEndGround


execute @s[tag=hc_end] ~ ~ ~ execute @e[type=ender_dragon] ~ ~ ~ scoreboard players tag @a[tag=hc_end] add hc_onEndGround {OnGround:1b}

#summon area_effect_cloud ~ ~ ~ {Tags:["hc_test_aec"],Particle:"take"}

#stats entity @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] set SuccessCount @s hc_dragon_alive
#scoreboard players set @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_dragon_alive 0
#execute @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] ~ ~ ~ testfor @e[type=ender_dragon]

#scoreboard players operation @s hc_dragon_alive = @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_dragon_alive


scoreboard players tag @s[tag=hc_dragonAlive] remove hc_dragonAlive
execute @s[tag=hc_end] ~ ~ ~ execute @e[type=ender_dragon,r=200] ~ ~ ~ scoreboard players tag @a[r=200] add hc_dragonAlive


execute @s[tag=hc_end] ~ ~ ~ scoreboard players tag @s[tag=!hc_dragonAlive] remove hc_onEndGround

kill @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec]