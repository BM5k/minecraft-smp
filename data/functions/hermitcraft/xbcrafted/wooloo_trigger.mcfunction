
summon area_effect_cloud ~ ~ ~ {Tags:["hc_test_aec"],Particle:"take"}

stats entity @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] set AffectedItems @s hc_nametag_5
scoreboard players set @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_nametag_5 0
execute @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] ~ ~ ~ /clear @a[c=1,r=0] minecraft:name_tag 0 0

scoreboard players operation @s hc_nametag_5 -= @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_nametag_5

execute @s[score_hc_nametag_5_min=1] ~ ~ ~ scoreboard players tag @e[type=evocation_illager,name=Wololo,tag=!hc_processed,r=10] add hc_process
execute @e[type=evocation_illager,name=Wololo,tag=hc_process,r=10] ~ ~ ~ advancement grant @a[r=10,score_hc_nametag_5_min=1] only hermitcraft:xbcrafted/wooloo
scoreboard players tag @e[type=evocation_illager,name=Wololo,tag=hc_process] add hc_processed


scoreboard players operation @s hc_nametag_5 = @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_nametag_5


advancement revoke @s only hermitcraft:xbcrafted/wooloo_trigger

kill @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec]