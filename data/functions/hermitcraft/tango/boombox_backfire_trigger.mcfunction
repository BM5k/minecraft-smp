#remove tag
scoreboard players tag @s[tag=hc_nearTnt] remove hc_nearTnt

#check for TNT that's about to explode
scoreboard players tag @e[type=tnt,r=10] add hc_aboutToExplode {Fuse:1s}

#tag all players that are close to one.
execute @e[type=tnt,tag=hc_aboutToExplode] ~ ~ ~ scoreboard players tag @a[r=7] add hc_nearTnt


advancement revoke @s only hermitcraft:tango/boombox_backfire_trigger