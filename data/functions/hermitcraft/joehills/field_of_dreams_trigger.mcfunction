scoreboard players tag @e[type=item,tag=!hc_processed,r=7] add hc_wheat {Item:{id:"minecraft:wheat",Count:1b},Age:0s,PickupDelay:10s}

execute @e[type=item,tag=hc_wheat,r=7] ~ ~ ~ scoreboard players add @a[r=7] hc_harvest_wheat 1

scoreboard players tag @e[type=item,tag=hc_wheat,r=7] add hc_processed
scoreboard players tag @e[type=item,tag=hc_wheat,r=7] remove hc_wheat

advancement grant @s[score_hc_harvest_wheat_min=1919] only hermitcraft:joehills/field_of_dreams

advancement revoke @s[score_hc_harvest_wheat=1918] only hermitcraft:joehills/field_of_dreams_trigger
