advancement grant @s[score_hc_fall_min=300,score_hc_death3_min=1] only hermitcraft:vintage/beefs_knees


advancement revoke @s[score_hc_fall=299] only hermitcraft:vintage/beefs_knees_trigger
advancement revoke @s[score_hc_death3=0] only hermitcraft:vintage/beefs_knees_trigger

scoreboard players set @s[score_hc_death3_min=1] hc_death3 0
scoreboard players set @s[score_hc_fall_min=1] hc_fall 0