
summon area_effect_cloud ~ ~ ~ {Tags:["hc_test_aec"],Particle:"take"}

stats entity @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] set AffectedItems @s hc_wool_in_inv
scoreboard players set @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_wool_in_inv 0


execute @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] ~ ~ ~ /clear @a[c=1,r=0] minecraft:wool -1 0
scoreboard players operation @s hc_wool_in_inv = @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec] hc_wool_in_inv


advancement grant @s[score_hc_wool_in_inv_min=1000] only hermitcraft:ijevin/pink_death
advancement revoke @s[score_hc_wool_in_inv=999] only hermitcraft:ijevin/pink_death_trigger


kill @e[type=area_effect_cloud,c=1,r=0,tag=hc_test_aec]