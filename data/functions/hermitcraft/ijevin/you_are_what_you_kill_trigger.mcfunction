#if (hc_slimes_killed = 1000) grant advancement
#since the function runs before the scoreboard counts up, we actually need to check for 999 instead of 1000.

advancement grant @s[score_hc_slimes_killed_min=999] only hermitcraft:ijevin/you_are_what_you_kill
advancement revoke @s[score_hc_slimes_killed=998] only hermitcraft:ijevin/you_are_what_you_kill_trigger