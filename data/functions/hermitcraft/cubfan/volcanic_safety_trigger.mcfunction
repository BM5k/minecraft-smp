
execute @s ~ ~ ~ detect ~ ~-1 ~ minecraft:magma * scoreboard players tag @s add hc_volcanic_safety {Inventory:[{Slot:100b,tag:{ench:[{id:9s}]}}]}


advancement grant @s[tag=hc_volcanic_safety] only hermitcraft:cubfan/volcanic_safety

advancement revoke @s only hermitcraft:cubfan/volcanic_safety_trigger


scoreboard players tag @s[tag=hc_volcanic_safety] remove hc_volcanic_safety
