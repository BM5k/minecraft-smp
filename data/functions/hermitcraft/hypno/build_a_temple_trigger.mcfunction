scoreboard players add @s hc_stone_brick 1

advancement grant @s[score_hc_stone_brick_min=100] only hermitcraft:hypno/build_a_temple

advancement revoke @s[score_hc_stone_brick=99] only hermitcraft:hypno/build_a_temple_trigger
