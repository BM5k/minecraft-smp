scoreboard players add @s hc_cured 1

advancement grant @s[score_hc_cured_min=1] only hermitcraft:docm/doctor_undead cured_one
advancement grant @s[score_hc_cured_min=2] only hermitcraft:docm/doctor_undead cured_two
advancement grant @s[score_hc_cured_min=3] only hermitcraft:docm/doctor_undead cured_three
advancement grant @s[score_hc_cured_min=4] only hermitcraft:docm/doctor_undead cured_four
advancement grant @s[score_hc_cured_min=5] only hermitcraft:docm/doctor_undead cured_five



advancement revoke @s[score_hc_cured=4] only hermitcraft:docm/doctor_undead_trigger