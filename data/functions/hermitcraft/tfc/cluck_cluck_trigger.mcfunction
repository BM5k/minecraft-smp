scoreboard players add @s hc_eat_chicken 1

advancement grant @s[score_hc_eat_chicken_min=64] only hermitcraft:tfc/cluck_cluck

advancement revoke @s[score_hc_eat_chicken=63] only hermitcraft:tfc/cluck_cluck_trigger
