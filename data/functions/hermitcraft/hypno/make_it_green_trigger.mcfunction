scoreboard players add @s hc_emerald_block 1

advancement grant @s[score_hc_emerald_block_min=10] only hermitcraft:hypno/make_it_green

advancement revoke @s[score_hc_emerald_block=9] only hermitcraft:hypno/make_it_green_trigger
